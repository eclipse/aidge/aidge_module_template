#include "aidge/example.hpp"

////////////////////////////////////////////////////////////////////////////////
// Example 1:
int Aidge::example_func(std::shared_ptr<Aidge::GraphView> g) {
    return g->getNodes().size();
}
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
// Example 2:
void Aidge::ReLUImpl_cpu_custom::forward() {
    const ReLU_Op& op_ = dynamic_cast<const ReLU_Op&>(mOp);
    // This custom implementation only handles float input and output
    assert(op_.getInput(0)->dataType() == DataType::Float32 && "Requires float input");
    assert(op_.getOutput(0)->dataType() == DataType::Float32 && "Requires float output");

    // Compute the ReLU
    const float* input = static_cast<const float*>(op_.getInput(0)->getImpl()->rawPtr());
    float* output = static_cast<float*>(op_.getOutput(0)->getImpl()->rawPtr());

    for (std::size_t i = 0; i < op_.getInput(0)->size(); ++i) {
        output[i] = input[i] > 0 ? input[i] : 0;
    }


}
////////////////////////////////////////////////////////////////////////////////
