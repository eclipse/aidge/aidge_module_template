'''
Guide line for testing

The file test file should be placed in the test folder and should be named following the convention : "test_*.py".

The command to test the library is (you have to be in N2D2/python) :

python -m unittest discover -s tests -v

The option discovery check for test_*.py files, so for example this file will note be caught !


If you need more information please check : https://docs.python.org/3/library/unittest.html
'''

import unittest

import aidge_core
import aidge_module_template

class test_example(unittest.TestCase):
    """
    The class needs to inherit unittest.TestCase, the name doesn't matter and the class doesn't need to be instantiated.
    """

    def setUp(self):
        """
        Method called before each test
        """
        pass

    def tearDown(self):
        """
        Method called after a test, even if it failed.
        Can be used to clean variables
        """
        pass

    def test_example(self):
        """
        Method called to test a functionality. It needs to be named test_* to be called.
        """
        g = aidge_core.GraphView()
        g.add(aidge_core.Conv2D(1, 16, [3,3], "conv"))

        self.assertTrue(aidge_module_template.example_func(g) == 3)
        self.assertTrue(aidge_module_template.example_func_python(g) == 3)

if __name__ == '__main__':
    """
    You need to add this line for the tests to be run.
    """
    unittest.main()