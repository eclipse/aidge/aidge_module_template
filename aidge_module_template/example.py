import aidge_core

# Example 3: same a example 1 but in Python
def example_func_python(g: aidge_core.GraphView):
    return len(g.get_nodes())
