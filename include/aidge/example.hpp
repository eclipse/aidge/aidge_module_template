/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef __AIDGE_MODULE_TEMPLATE_EXAMPLE_H__
#define __AIDGE_MODULE_TEMPLATE_EXAMPLE_H__

#include <aidge/aidge.hpp>
#include <aidge/backend/cpu.hpp>

namespace Aidge {

////////////////////////////////////////////////////////////////////////////////
// Example 1: just a new function that displays the number of nodes in a 
// GraphView. This function is also binded in Python, have a look at 
// python_binding/pybind_module_template.cpp to see how.
int example_func(std::shared_ptr<Aidge::GraphView> g);
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
// Example 2: a custom implementation for the ReLU operator, that uses the Aidge 
// plugin mechanism.
class ReLUImpl_cpu_custom : public ReLUImpl_cpu {
   public:
    ReLUImpl_cpu_custom(const ReLU_Op& op) : ReLUImpl_cpu(op) {}

    void forward();
};

namespace {
static Registrar<ReLU_Op> registrarReLUImpl_cpu_custom("cpu_custom",
    Aidge::ReLUImpl_cpu_custom::create);
}
////////////////////////////////////////////////////////////////////////////////

}  // namespace Aidge

#endif /* __AIDGE_MODULE_TEMPLATE_EXAMPLE_H__ */
