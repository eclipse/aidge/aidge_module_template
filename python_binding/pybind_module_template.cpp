#include <pybind11/pybind11.h>
#include "aidge/example.hpp"

////////////////////////////////////////////////////////////////////////////////
// Example 2: in order to automatically register the cpu_custom ReLU
// backend, the backend header must be included!
#include "aidge/backend/cpu.hpp"
////////////////////////////////////////////////////////////////////////////////

namespace py = pybind11;

namespace Aidge {
void init_AidgeModuleTemplate(py::module& m){
  //////////////////////////////////////////////////////////////////////////////
  // Example 1: bind example_func
  m.def("example_func", &example_func, py::arg("g"), R"mydelimiter(
    Returns the number of nodes in a computing graph.

    :param g: The computing graph.
    :type g: :py:class:`aidge_core.GraphView`
    :returns: Number of nodes in the graph
    :rtype: int

    )mydelimiter");
  //////////////////////////////////////////////////////////////////////////////
}

PYBIND11_MODULE(aidge_module_template, m) {
    init_AidgeModuleTemplate(m);
}
}
