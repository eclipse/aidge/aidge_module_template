/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/example.hpp"

using namespace Aidge;

////////////////////////////////////////////////////////////////////////////////
// Example 1: test case for function example_func()
TEST_CASE("[module_template] example_func") {
    SECTION("Run") {
        auto g = Sequential({
            Conv(1, 16, {3, 3}, "conv1"),
            Conv(16, 32, {3, 3}, "conv2"),
            Conv(32, 64, {3, 3}, "conv3"),
            FC(10, false, "Fc")
        });

        // There is 12 nodes in this graph:
        // - 4 nodes for the compute layers (3x Conv + 1x FC)
        // - 4 nodes for the associated weights
        // - 4 nodes for the associated biases
        REQUIRE(example_func(g) == 12);
    }
}
////////////////////////////////////////////////////////////////////////////////
